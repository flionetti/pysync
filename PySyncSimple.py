import json
settings = json.loads(open("settings.json", 'r').read())
server = settings['server']
username = settings['username']
password = settings['password']
local_directory = settings['local_directory']
remote_directory = settings['remote_directory']

import os
import paramiko# pip3.7 install paramiko 
import time

base_dir, base_name = os.path.split(local_directory)
local_zip = local_directory + '.tgz'

print("compressing...")
start_time = time.time()
import tarfile
print("will write to: '%s'"%local_zip)
tar = tarfile.open(local_zip, "w:gz")
os.chdir(base_dir)
tar.add(base_name, filter=lambda x: None if '.git' in x.name else x)
tar.close()

end_time = time.time()
print("Took %.2fs to compress"%(end_time-start_time))
remote_path = remote_directory + "/" + base_name
remote_zip = remote_path + ".tgz"
tgz_file_name = base_name + ".tgz"

if os.path.exists(local_zip):
    ssh = paramiko.SSHClient()
    password = password
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username=username, password=password)
    sftp = ssh.open_sftp()
    
    print("sftp '%s' to '%s'"%(local_zip, remote_zip))
    start_time = time.time()
    sftp.put(local_zip, remote_zip)
    sftp.close()
    end_time = time.time()
    print("Took %.2fs to transfer"%(end_time-start_time))
    
    start_time = time.time()
    cmd_to_execute = "cd %s;tar -xzvf %s >>log.txt 2>&1"%(remote_directory, tgz_file_name)
    print("cmd_to_execute:", cmd_to_execute)
    stdin,stdout,stderr=ssh.exec_command(cmd_to_execute)
    end_time = time.time()
    print("Took %.2fs to decompress"%(end_time-start_time))
