#TODO: on startup, zip the whole folder, copy it over, and then unzip
#TODO: not handling folders or renames correctly
#TODO: not handling new files correctly???

import json
settings = json.loads(open("settings.json", 'r').read())
server = settings['server']
username = settings['username']
password = settings['password']
local_directory = settings['local_directory']
remote_directory = settings['remote_directory']

import watchdog# pip3.7 install watchdog 
import paramiko# pip3.7 install paramiko 

import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler

ssh = paramiko.SSHClient()
password = password
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(server, username=username, password=password)
sftp = ssh.open_sftp()
import traceback


class Event(LoggingEventHandler):
    def dispatch(self, event):
        print("Foobar", event.event_type, event.src_path)
        changed_file = event.src_path
        if hasattr(event, "dest_path"):
            print( "dest_path:", event.dest_path )
            changed_file = event.src_path
        
        local_path = changed_file.split(local_directory)[-1][1:]
        print("local_path:", local_path)
        new_path = remote_directory + "/" + local_path
        print ("copy '%s' to '%s'"%(changed_file, new_path))
        try:
            sftp.put(changed_file, new_path)
        except:
            print("Failed to copy!")
            traceback.print_exc()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = local_directory
    event_handler = Event()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()